
package api;

import io.cucumber.core.internal.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.List;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Article {

    private String bite;
    private List<Object> categories;
    private String content;
    private String date;
    private String excerpt;
    private Boolean featured;
    private String id;
    private String lang;
    private String layout;
    private String metaDescription;
    private String metaTitle;
    private String path;
    private Boolean published;
    private List<Object> related;
    private String seoKeywords;
    private Long sort;
    private List<Object> tags;
    private String title;
    private String titleShort;
    private List<Object> topics;
    private String url;

}
